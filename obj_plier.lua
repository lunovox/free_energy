free_energy.pliers = {
	getDescription = function()
		return core.colorize("#00ff00", free_energy.translate("Plier of Electrical Energy"))
			.."\n\t * "..free_energy.translate("Tool that remakes electrical connections between electrical equipment.")
	end,
}
minetest.register_craftitem("free_energy:plier", {
	description = free_energy.pliers.getDescription(),
	inventory_image = "icon_plier.png",
	on_use = function(itemstack, player, pointed_thing)
	--on_rightclick = function(pos, node, player, itemstack, pointed_thing) --on_rightclick não funciona!!!!
		local playername = player:get_player_name()
		local pos = pointed_thing.under
		
		if type(pos)~="table" then return nil end
		if pos.x==nil or type(pos.x)~="number" then return nil end
		if pos.y==nil or type(pos.y)~="number" then return nil end
		if pos.z==nil or type(pos.z)~="number" then return nil end
		
		local node = minetest.get_node(pos)
		local ownername = minetest.get_meta(pos):get_string("owner");
		--node.param2 = dir_to_facedir(dir, true)
		--minetest.set_node(pos, node)
		
		if type(free_energy.pointed) == "nil" then
			free_energy.pointed = pos
			minetest.chat_send_player(playername, 
				core.colorize("#00ff00", "["..free_energy.translate("PLIER").."] ")
				..free_energy.translate("Connect another device!")
			)
			free_energy.playSoundEfect("sfx_plier", pos)
		else
			if node.name == "free_energy:recharge_controller" 
				or node.name == "free_energy:solar_panel" 
				or node.name == "free_energy:stationary_battery" 
			then
				--minetest.chat_send_player(playername, "node.name = "..dump(node.name))
				local dist = free_energy.getDistance(free_energy.pointed, pos)
				if dist >= 1 then
					if dist<= 32 then
						local posController, posDevice, objDevice
						if minetest.get_node(free_energy.pointed).name == "free_energy:recharge_controller" then
							posController = free_energy.pointed
							posDevice = pos
							objDevice = minetest.get_node(pos).name
						elseif minetest.get_node(pos).name == "free_energy:recharge_controller" then
							posController = pos
							posDevice = free_energy.pointed
							objDevice = minetest.get_node(free_energy.pointed).name
						else
							posDevice = free_energy.pointed
							objDevice = minetest.get_node(free_energy.pointed).name
						end
						--minetest.chat_send_player(playername, "posController = "..dump(posController))
						--minetest.chat_send_player(playername, "posDevice = "..dump(posDevice))
						
						local nameDevice, typeDevice
						if objDevice == "free_energy:solar_panel" then
							nameDevice = free_energy.translate("Solar Painel")
							typeDevice = "Sources"
						elseif objDevice == "free_energy:stationary_battery" then
							nameDevice = free_energy.translate("Stationary Battery")
							typeDevice = "Stores"
						else
							nameDevice = free_energy.translate("Know Device").." "..minetest.pos_to_string(posDevice)
							typeDevice = free_energy.translate("Know Type")
						end
						--minetest.chat_send_player(playername, "objDevice = "..dump(objDevice))
					
						if type(posController) ~= "nil" then
							if type(ownername)=="string" and ownername:trim()~="" then
								if playername == ownername then
								
									local metaController = minetest.get_meta(posController);
									local metaDevice = minetest.get_meta(posDevice);
									local connsController = metaController:get_string("con"..typeDevice):split(";")
									local connsDevice= metaDevice:get_string("connections"):split(";")
									--minetest.chat_send_player(playername, "connsController = "..dump(#connsController))
									--minetest.chat_send_player(playername, "connsDevice = "..dump(#connsDevice))
									local isExist = false
									local newConnController = ""
									
									if typeDevice == "Sources" or typeDevice == "Stores" then
										--minetest.chat_send_player(playername, "connsController = "..dump(connsController))
										for i, strConnController in pairs(connsController) do
											--minetest.chat_send_player(playername, "("..i..") strConnController = "..dump(strConnController))
											if strConnController == minetest.pos_to_string(posDevice) then
												isExist = true
											else
												if newConnController == "" then
													newConnController = minetest.pos_to_string(posDevice)
												else
													newConnController = newConnController..";"..minetest.pos_to_string(posDevice)
												end
											end
										end
										--minetest.chat_send_player(playername, "(Controller).connections = "..newConnController)
									elseif typeDevice == "Consumers" then
									
									end
									
									local newConnDevice = ""
									--minetest.chat_send_player(playername, "connsDevice = "..dump(connsDevice))
									for i, strConnDevice in pairs(connsDevice) do
										--minetest.chat_send_player(playername, "("..i..") strConnDevice = "..dump(strConnDevice))
										if strConnDevice == minetest.pos_to_string(posController) then
											isExist = true
										else
											if newConnDevice == "" then
												newConnDevice = minetest.pos_to_string(posController)
											else
												newConnDevice = newConnDevice..";"..minetest.pos_to_string(posController)
											end
										end
									end
									--minetest.chat_send_player(playername, "("..nameDevice..").connections = "..newConnDevice)
									
									if isExist == true then
										--undo connection
										metaController:set_string("con"..typeDevice, newConnController)
										metaDevice:set_string("connections", newConnDevice)
										free_energy.playSoundEfect("sfx_plier", pos)
										minetest.chat_send_player(playername, 
											core.colorize("#00ff00", "["..free_energy.translate("PLIER").."] ")
											..(free_energy.translate("You have disconnected device '@1' from the Recharge Controller!", nameDevice))
										)
										free_energy.doSparkle(pointed_thing.under)
									else
										--do connection
										metaController:set_string("con"..typeDevice, newConnController..";"..minetest.pos_to_string(posDevice))
										metaDevice:set_string("connections", newConnDevice..";"..minetest.pos_to_string(posController))
										free_energy.playSoundEfect("sfx_plier", pos)
										minetest.chat_send_player(playername, 
											core.colorize("#00ff00", "["..free_energy.translate("PLIER").."] ")
											..(free_energy.translate("You have connected the Charge Controller to device '@1'!", nameDevice))
										)
										free_energy.doSparkle(pointed_thing.under)
									end
									free_energy.pointed = nil
								else --ELSE NOT: if playername == ownername then
									minetest.chat_send_player(playername, 
										core.colorize("#ff0000", "["..free_energy.translate("PLIER").." (ERROR)] ")
										..free_energy.translate("Cannot connect to the '%s' that belongs to '%s'!"):format(nameDevice, ownername)
									)
									free_energy.playSoundEfect("sfx_plier", pos)
								end
							else --ELSE NOT: if type(ownername)=="string" and ownername:trim()~="" then
								minetest.chat_send_player(playername, 
									core.colorize("#ff0000", "["..free_energy.translate("PLIER").." (ERROR)] ")
									..free_energy.translate("Cannot connect to '%s' because you do not have a registered owner!"):format(nameDevice)
								)
								free_energy.playSoundEfect("sfx_plier", pos)
							end
						else
							--Substituir este código por fogo!
							free_energy.playSoundEfect("sfx_plier", pos)
							minetest.chat_send_player(playername, 
								core.colorize("#ff0000", "["..free_energy.translate("PLIER").." (ERROR)] ")
								..free_energy.translate("Inappropriate connection!")
							)
							free_energy.pointed = nil
							free_energy.doSparkle(pos)
							minetest.set_node(pos, {name="fire:basic_flame"})
						end
					else
						minetest.chat_send_player(playername, 
							core.colorize("#ff0000", "["..free_energy.translate("PLIER").." (ERROR)] ")
							..free_energy.translate("Impossible to connect device over a distance of %02d meters!"):format(free_energy.maxConnectedDistance)
						)
						free_energy.playSoundEfect("sfx_plier", pos)
					end
				else
					minetest.chat_send_player(playername, 
						core.colorize("#00ff00", "["..free_energy.translate("PLIER").."] ")
						..free_energy.translate("Select another device to connect!")
						--..(" (dist = %0.8f)"):format(dist)
					)
					free_energy.playSoundEfect("sfx_plier", pos)
				end
			else
				minetest.chat_send_player(playername, 
					core.colorize("#ff0000", "["..free_energy.translate("PLIER").." (ERROR)] ")
					..free_energy.translate("It is impossible to connect this!")
				)
				free_energy.playSoundEfect("sfx_plier", pos)
			end
		end --if type(free_energy.pointed) == "nil" then
		
	end, --on_use = function(itemstack, player, pointed_thing)
})
