# Free Energy

[Minetest mod] Generate and consume your own energy for energy through solar panels.

## Screenshot:

![screenshot]

Outras Screnshots: [screenshot2] | [screenshot3]

## Depends:
 * **default** (mod of standard game)
 * **[intllib]** (optional) ← Internationalization library for Minetest. 
 
## Licence: 
 * [GNU AGPL]

## Repositories:
 * [Free Energy].

## Developers:
 * [Lunovox Heavenfinder] ← for code and portuguese translate.

## Languages:
 * en ← [English] (default language, no need extra mod).
 * pt ← [Portuguese] per command ````/set language pt````. Need mod '**[intllib]**' to work.

Contact a developer of this mod to add a translate to your language!

[screenshot]:https://gitlab.com/lunovox/free_energy/-/raw/master/screenshot.png
[screenshot2]:https://gitlab.com/lunovox/free_energy/-/raw/master/screenshot2.png
[screenshot3]:https://gitlab.com/lunovox/free_energy/-/raw/master/screenshot3.png
[GNU AGPL]:https://gitlab.com/lunovox/free_energy/-/raw/master/LICENSE
[intllib]:https://github.com/minetest-mods/intllib
[Free Energy]:https://gitlab.com/lunovox/free_energy.git
[Lunovox Heavenfinder]:https://libreplanet.org/wiki/User:Lunovox
[English]:https://gitlab.com/lunovox/free_energy/-/raw/master/locale/template.pot?inline=false
[Portuguese]:https://gitlab.com/lunovox/free_energy/-/raw/master/locale/pt.po?inline=false
