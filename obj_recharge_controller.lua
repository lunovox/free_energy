free_energy.recharge_controller = {
	getGroup = function(id)
		--and not core.setting_getbool("creative_mode")
		if id == "minetest" then
			return {snappy=3, dig_immediate=3, attached_node=1}
		else
			return {snappy=3,flammable=2,dig_immediate=3, attached_node=1, not_in_creative_inventory=1}
		end
	end,
	node = {
		type = "fixed",
		fixed = {
			{-0.4375,-0.4375, -0.4375,		0.4375,0.4375,0.4375}, --Corpo principal
		},
	},
	tiles = {
		--"default_dirt.png^[colorize:#802BB177",
		"tex_recharge_controller_side.png",  --Cima
		"tex_recharge_controller_side.png",  --Baixo
		"tex_recharge_controller_side.png",  --Esqueda
		"tex_recharge_controller_side.png",  --Direita
		"tex_recharge_controller_side.png",  --Traz
		"tex_recharge_controller_from.png^tex_recharge_controller_offline.png", --Frente
	},
	getDescription = function()
		return core.colorize("#00ff00", free_energy.translate("Recharge Controler (12V 360A 4320W)"))
			.."\n\t * "..free_energy.translate("Controls battery recharging and transforms direct current into alternating current.")
	end,
}

minetest.register_node("free_energy:recharge_controller",{
	description = free_energy.recharge_controller.getDescription(),
	stack_max = 1, 
	drawtype = "nodebox",
	node_box = free_energy.recharge_controller.node,
	selection_box = free_energy.recharge_controller.node,
	tiles = free_energy.recharge_controller.tiles,
	paramtype = "light", --Nao sei pq, mas o blco nao aceita a luz se nao tiver esta propriedade
	paramtype2 = "facedir", --Tem a face virada para o jogador!
	--walkable = false,
	groups = free_energy.recharge_controller.getGroup("minetest"),
	sounds = default.node_sound_wood_defaults(),
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local playername = placer:get_player_name()
		local meta = minetest.get_meta(pos);
		meta:set_string("infotext", free_energy.recharge_controller.getDescription())
		meta:set_string("owner", playername)
		meta:set_string("conSources", "") --Connections with Sources (Solar Panels)
		meta:set_string("conStores", "") --Connections with Stores (Stationary Batteries)
		meta:set_string("conConsumers", "")--Connections with Consumers (Power Consumers)
	end,
})
