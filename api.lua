free_energy = { 
	maxConnectedDistance = 32,
	getDistance = function (pos1, pos2)
		if pos1==nil or pos2==nil then return nil end
		if pos1.x==nil or type(pos1.x)~="number" then return nil end
		if pos1.y==nil or type(pos1.y)~="number" then return nil end
		if pos1.z==nil or type(pos1.z)~="number" then return nil end
		if pos2.x==nil or type(pos2.x)~="number" then return nil end
		if pos2.y==nil or type(pos2.y)~="number" then return nil end
		if pos2.z==nil or type(pos2.z)~="number" then return nil end
		return math.sqrt( ((pos1.x-pos2.x)^2) + ((pos1.y-pos2.y)^2) + ((pos1.z-pos2.z)^2) )
	end,
	playSoundEfect = function (sfxName, pos) --SAMPLE: free_energy.playSoundEfect("sfx_connection", pos)
		minetest.sound_play(
			sfxName, {
				--object = self.object, --toca so em volta do disco.
				--object = player, --Som ruim.
				--to_player = playername, --Toca somente a cabeça de um unico jogador.
				--pos = player:getpos(), --Tocaca cabeca do jogador e em quem estiver proximo 8 blocos (max_hear_distance)
				pos = pos,
				gain = 2.0, 
				max_hear_distance = 8
			}
		)
	end,
	doSparkle = function (pos) --SAMPLE: free_energy.doSparkleParticle(pos)
		minetest.add_particlespawner({
			amount = 64,
			time = 1,
			minpos = {x=pos.x-0.3, y=pos.y+0.2, z=pos.z-0.3},
			maxpos = {x=pos.x+0.3, y=pos.y+0.7, z=pos.z+0.3},
			minvel = {x=-0.3,	y=1.0,	z=-0.3},
			maxvel = {x=0.3,	y=3.0,	z=0.3},
			minacc = {x=0, y=0, z=0},
			maxacc = {x=0, y=0, z=0},
			minexptime = 0.25,
			maxexptime = 1.50,
			minsize = 1,
			maxsize = 3,
			collisiondetection = false,
			vertical = true,
			texture = "tex_particle_blue.png",
			--playername = "singleplayer",
		})
		free_energy.playSoundEfect("sfx_sparkles", pos)
	end,
}
