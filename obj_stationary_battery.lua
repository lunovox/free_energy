free_energy.stationary_battery = {
	getGroup = function(id)
		--and not core.setting_getbool("creative_mode")
		if id == "minetest" then
			return {snappy=3, dig_immediate=3, attached_node=1}
		else
			return {snappy=3,flammable=2,dig_immediate=3, attached_node=1, not_in_creative_inventory=1}
		end
	end,
	node = {
		type = "fixed",
		fixed = {
			{-0.4375,0.25, -0.4375,		 0.4375,-0.5,0.4375}, --Corpo principal
			{-0.3750,0.375,-0.09375,	-0.1875,0.25,0.09375}, --Pino 1
			{ 0.3750,0.375,-0.09375,	 0.1875,0.25,0.09375}, --Pino 2
		}
	},
	tiles = {
		--"default_dirt.png^[colorize:#802BB177",
		"tex_stationary_battery_top.png",  --Cima
		"tex_stationary_battery_bottom.png",  --Baixo
		"tex_stationary_battery_side.png",  --Esqueda
		"tex_stationary_battery_side.png",  --Direita
		"tex_stationary_battery_side.png",  --Traz
		"tex_stationary_battery_side.png^tex_stationary_battery_simbol.png", --Frente
	},
	getDescription = function()
		return core.colorize("#00ff00", free_energy.translate("Stationary Battery (12V 150Ah 1800Wh)"))
			.."\n\t * "..free_energy.translate("Stores direct current electrical energy.")
	end,
}

minetest.register_node("free_energy:stationary_battery",{
	description = free_energy.stationary_battery.getDescription(),
	stack_max = 1, 
	drawtype = "nodebox",
	node_box = free_energy.stationary_battery.node,
	selection_box = free_energy.stationary_battery.node,
	tiles = free_energy.stationary_battery.tiles,
	paramtype = "light", --Nao sei pq, mas o blco nao aceita a luz se nao tiver esta propriedade
	paramtype2 = "facedir", --Tem a face virada para o jogador!
	--walkable = false,
	groups = free_energy.stationary_battery.getGroup("minetest"),
	sounds = default.node_sound_wood_defaults(),
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local playername = placer:get_player_name()
		local meta = minetest.get_meta(pos);
		meta:set_string("infotext", free_energy.stationary_battery.getDescription())
		meta:set_string("owner", playername)
		meta:set_string("connections", "")
		--meta:set_number("minCharge", 0)
		--meta:set_number("maxCharge", 1800) --in Watts
	end,
})
