local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)


dofile(modpath.."/api.lua")
dofile(modpath.."/translate.lua")
dofile(modpath.."/obj_solar_panel.lua")
dofile(modpath.."/obj_stationary_battery.lua")
dofile(modpath.."/obj_recharge_controller.lua")
dofile(modpath.."/obj_plier.lua")


minetest.log('action',"["..modname:upper().."] Carregado!")
