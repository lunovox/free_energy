
free_energy.solar_panel = {
	getGroup = function(id)
		if id == "minetest" then
			return {snappy=3, dig_immediate=3, attached_node=1}
		else
			return {snappy=3,flammable=2,dig_immediate=3, attached_node=1, not_in_creative_inventory=1}
		end
	end,
	node = {
		type = "fixed",
		fixed = {
			{-0.5,-0.4375,-0.5,	0.5,-0.5,0.5}, --Tabua principal
			--[[
			{-0.5,0.6875,0.5,	0.5,0.5,0.4375}, --Tabua de Tomadas
			{-0.4375,0.1875,-0.4375,	0.4375,0.4375,0.4375}, --Gaveta

			{-0.0625,0.3437,-0.4375,	0.0625,0.2812,-0.5}, --Puchador


			{-0.375,0.4375,-0.375,	-0.25,-0.5,-0.25}, --Perna
			{-0.375,0.4375,0.375,	-0.25,-0.5,0.25}, --Perna
			{0.375,0.4375,-0.375,	0.25,-0.5,-0.25}, --Perna
			{0.375,0.4375,0.375,		0.25,-0.5,0.25}, --Perna
			--]]
		}
	},
	tiles = {
		--"default_dirt.png^[colorize:#802BB177",
		"tex_solar_panel.png",  --Cima
		"tex_stationary_battery_top.png",  --Baixo
		"tex_stationary_battery_bottom.png",  --Esqueda
		"tex_stationary_battery_bottom.png",  --Direita
		"tex_stationary_battery_bottom.png",  --Traz
		"tex_stationary_battery_bottom.png", --Frente
	},
	getDescription = function()
		return core.colorize("#00ff00", free_energy.translate("Perovskitas Solar Panel (12V 30A 360W)"))
			.."\n\t * "..free_energy.translate("It transforms solar energy into direct current electricity as long as there is strong sun.")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local playername = player:get_player_name()
		local overpos = {x=(pos.x), y=(pos.y+1), z=(pos.z)}
		local luz = minetest.get_node_light(overpos)
		local hora = minetest.get_timeofday()*24
		-- [Luz acima de 7] = com sol | só o sol tem [luz = 15]
		local isOn = false
		if hora >= 6 and hora <= 17 and luz >= 15 then
			isOn = true
		else
			isOn = false
		end
		minetest.chat_send_player(playername, "[free_energy] Hora = "..hora.." | Luz = "..luz.." | isOn = "..dump(isOn))
	end,
}

minetest.register_node("free_energy:solar_panel",{
	description = "Solar Panel",
	description = free_energy.solar_panel.getDescription(),
	stack_max = 1, 
	drawtype = "nodebox",
	node_box = free_energy.solar_panel.node,
	selection_box = free_energy.solar_panel.node,
	tiles = free_energy.solar_panel.tiles,  
	--paramtype = "light", --Não pode ter isso or a placa solar nao sentira os 15 de luminosidade!
	--paramtype2 = "facedir", --Tem a face virada para o jogador!
	walkable = false,
	groups = free_energy.solar_panel.getGroup("minetest"),
	sounds = default.node_sound_wood_defaults(),
	on_rightclick = free_energy.solar_panel.on_rightclick,
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local playername = placer:get_player_name()
		local meta = minetest.get_meta(pos);
		meta:set_string("infotext", free_energy.solar_panel.getDescription())
		meta:set_string("owner", playername)
		meta:set_string("connections", "")
	end,
})
